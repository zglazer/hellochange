package com.zachglazer.change;

public class SimpleChange {

    public static void main(String[] args) {
        SimpleGreedyArrayRegister register = new SimpleGreedyArrayRegister();
        ChangeMachineRunner changeMachineRunner = new ChangeMachineRunner(register);
        changeMachineRunner.run();
    }
}
