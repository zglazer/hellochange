package com.zachglazer.change;

import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.Scanner;

public class ChangeMachineRunner {

    private final SimpleGreedyArrayRegister register;
    private final Scanner scanner;

    public ChangeMachineRunner(SimpleGreedyArrayRegister register) {
        this.register = register;
        this.scanner = new Scanner(new InputStreamReader(System.in));
    }

    public void run() {
        boolean runProgram = true;
        System.out.println("ready");
        while (runProgram) {
            String input = getInput();
            String command = input.split(" ")[0];
            switch (command) {
                case "show":
                    printRegisterState();
                    break;
                case "put":
                    putCommand(input);
                    break;
                case "take":
                    takeCommand(input);
                    break;
                case "change":
                    changeCommand(input);
                    break;
                case "quit":
                    System.out.println("bye");
                    runProgram = false;
                    break;
                case "help":
                    printInstructions();
                    break;
                default:
                    System.out.println("unrecognized command");
                    printInstructions();
            }
        }
    }

    public String getInput() {
        return scanner.nextLine();
    }

    private void printRegisterState() {
        BigInteger total = register.getTotal();
        StringBuilder sb = new StringBuilder();
        for (int denomination : register.getBillCounts()) {
            sb.append(denomination).append(" ");
        }
        sb.deleteCharAt(sb.length() - 1);

        System.out.format("$%s %s\n", total, sb.toString());
    }

    private void changeCommand(String input) {
        String[] values = input.split("change ");
        if (values.length == 2) {
            try {
                BigInteger changeFor = new BigInteger(values[1]);
                int[] change = register.makeChange(changeFor);
                printArray(change);
            } catch (NumberFormatException e) {
                System.out.println("invalid number provided. only one integer is accepted");
            }  catch (IllegalArgumentException | IllegalStateException e) {
                System.out.println("error occurred during operation: " + e.getMessage());
            }
        } else {
            System.out.println("error: incorrect usage");
        }
    }

    private void putCommand(String input) {
        int[] inputCurrency;
        try {
            inputCurrency = parseCurrency(input);
            register.putCurrency(inputCurrency);
        } catch (NumberFormatException e) {
            System.out.println("invalid number provided. only integers are accepted");
        } catch (IllegalArgumentException e) {
            System.out.println("error occurred during operation: " + e.getMessage());
        }
        printRegisterState();
    }

    private void takeCommand(String input) {
        int[] inputCurrency;
        try {
            inputCurrency = parseCurrency(input);
            register.takeCurrency(inputCurrency);
        } catch (NumberFormatException e) {
            System.out.println("invalid number provided. only integers are accepted");
        } catch (IllegalArgumentException e) {
            System.out.println("error occurred during operation: " + e.getMessage());
        }
        printRegisterState();
    }

    private int[] parseCurrency(String rawInput) {
        String[] values = rawInput.split(" ");
        if (values.length != 6) {
            throw new IllegalArgumentException("invalid input");
        }
        int[] currency = new int[5];
        for (int i = 0; i < currency.length; i++) {
            currency[i] = Integer.parseInt(values[i + 1]);
        }
        return currency;
    }

    private void printArray(int[] arr) {
        StringBuilder sb = new StringBuilder();
        for (int val : arr) {
            sb.append(val).append(" ");
        }
        sb.deleteCharAt(sb.length() - 1);
        System.out.println(sb.toString());
    }

    private void printInstructions() {
        System.out.println("possible commands: show, put, take, change, help, quit");
    }
}
