package com.zachglazer.change;

import java.math.BigInteger;
import java.util.Arrays;

public class SimpleGreedyArrayRegister {

    private int[] billCounts;
    private int[] billValues = { 20, 10, 5, 2, 1 };

    public SimpleGreedyArrayRegister() {
        this.billCounts = new int[5];
    }

    public int[] makeChange(BigInteger changeFor) {
        if (changeFor.compareTo(BigInteger.ZERO) < 0) {
            throw new IllegalArgumentException("negative amounts are not allowed");
        }
        BigInteger total = getTotal();
        if (changeFor.compareTo(total) > 0) {
            throw new IllegalArgumentException("there are not enough bills in the register for that");
        }

        int[] change = new int[5];
        int[] result = makeChangRecursive(change, 0, changeFor);
        if (result == null) {
            throw new IllegalStateException("we don't have the right bills in the register for that");
        }
        takeCurrency(result);
        return result;
    }

    private int[] makeChangRecursive(int[] change, int pos, BigInteger changeFor) {
        // base case -- exact change found
        if (changeFor.compareTo(BigInteger.ZERO) == 0) {
            return change;
        }

        // too much change given
        if (changeFor.compareTo(BigInteger.ZERO) < 0) {
            return null;
        }

        // out of denominations
        if (pos >= billValues.length) {
            return null;
        }

        // not enough available bills in the register
        if (change[pos] > billCounts[pos]) {
            return null;
        }

        int[] changeCopy = copy(change);
        if (billCounts[pos] > change[pos] && changeFor.subtract(BigInteger.valueOf(billValues[pos])).compareTo(BigInteger.ZERO) >= 0) {
            change[pos]++;
            BigInteger changeRemaining = changeFor.subtract(BigInteger.valueOf(billValues[pos]));

            int[] changeIfUseSameBill = makeChangRecursive(change, pos, changeRemaining);
            if (changeIfUseSameBill != null) {
                return changeIfUseSameBill;
            }
        }
        return makeChangRecursive(changeCopy, pos + 1, changeFor);
    }

    public void putCurrency(int[] inputCurrency) {
        validateInputCurrency(inputCurrency);
        for (int i = 0; i < inputCurrency.length; i++) {
            billCounts[i] += inputCurrency[i];
        }
    }

    public void takeCurrency(int[] inputCurrency) {
        validateInputCurrency(inputCurrency);
        int tmp[] = new int[5];
        System.arraycopy(billCounts, 0, tmp, 0, billCounts.length);
        for (int i = 0; i < inputCurrency.length; i++) {
            tmp[i] -= inputCurrency[i];
            if (tmp[i] < 0) {
                throw new IllegalArgumentException("there are not enough bills in the register for that");
            }
        }
        billCounts = tmp;
    }

    public BigInteger getTotal() {
        BigInteger total = BigInteger.ZERO;
        BigInteger product;
        for (int i = 0; i < billCounts.length; i++) {
            product = BigInteger.valueOf(billCounts[i]).multiply(BigInteger.valueOf(billValues[i]));
            total = total.add(product);
        }
        return total;
    }

    public int[] getBillCounts() {
        return billCounts;
    }

    private void validateInputCurrency(int[] inputCurrency) {
        if (inputCurrency.length > billCounts.length) {
            throw new IllegalArgumentException("register only accepts five bill denominations");
        }
        if (Arrays.stream(inputCurrency).anyMatch(amt -> amt < 0)) {
            throw new IllegalArgumentException("negative amounts are not allowed");
        }
    }

    private int[] copy(int[] src) {
        int[] copy = new int[src.length];
        System.arraycopy(src, 0, copy, 0, copy.length);
        return copy;
    }
}
