package com.zachglazer.change;

import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class SimpleGreedyArrayRegisterTest {

    private SimpleGreedyArrayRegister register;

    @Before
    public void setUp() {
        register = new SimpleGreedyArrayRegister();
    }

    @Test
    public void putLargeAmountsShouldAddUpToTotal() {
        int[] tenOfEach = {10, 10, 10, 10, 10};
        register.putCurrency(tenOfEach);
        BigInteger total = register.getTotal();

        assertThat(total, is(BigInteger.valueOf(380)));
        assertThat(register.getBillCounts(), is(equalTo(tenOfEach)));
    }

    @Test
    public void putVariousAmountsShouldAddTogether() {
        int[] oneOfEach = {1, 1, 1, 1, 1};
        int[] variousAmounts = {3, 40, 0, 4, 1};
        int[] expected = {4, 41, 1, 5, 2};

        register.putCurrency(variousAmounts);
        register.putCurrency(oneOfEach);

        assertThat(register.getBillCounts(), is(equalTo(expected)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void putDoesNotAllowNegativeValues() {
        int[] negativeCounts = {1, 2, 3, -1, 0};
        register.putCurrency(negativeCounts);
    }

    @Test(expected = IllegalArgumentException.class)
    public void putOnlyAllowsFiveSlots() {
        int[] tooManySlots = {1, 2, 3, 4, 5, 6};
        register.putCurrency(tooManySlots);
    }

    @Test
    public void takeShouldRemoveFromBillCounts() {
        int[] variousAmounts = {1, 3, 5, 0, 2};
        int[] takeAway = {1, 2, 0, 0, 2};
        int[] expected = {0, 1, 5, 0, 0};

        register.putCurrency(variousAmounts);
        register.takeCurrency(takeAway);

        assertThat(register.getBillCounts(), is(equalTo(expected)));
    }

    @Test
    public void takeLargeAmounts() {
        int[] largeAmounts = {100, 22, 5, 44, 9};
        int[] takeLargeAmount = {100, 20, 0, 43, 8};
        int[] expected = {0, 2, 5, 1, 1};

        register.putCurrency(largeAmounts);
        register.takeCurrency(takeLargeAmount);

        assertThat(register.getBillCounts(), is(equalTo(expected)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void takeDoesNotAllowMoreThanFiveSlots() {
        int[] tooManySlots = {1, 2, 3, 4, 5, 6};
        register.takeCurrency(tooManySlots);
    }

    @Test(expected = IllegalArgumentException.class)
    public void takeDoesNotAllowRemovingMoreThanBillCounts() {
        int[] variousAmounts = {1, 3, 5, 0, 2};
        int[] tooMuchToTake = {2, 0, 0, 1, 0};

        register.putCurrency(variousAmounts);
        register.takeCurrency(tooMuchToTake);
    }

    @Test
    public void takeTooMuchDoesNotChangeCounts() {
        int[] variousAmounts = {1, 3, 5, 0, 2};
        int[] tooMuchToTake = {2, 0, 0, 1, 0};

        register.putCurrency(variousAmounts);
        try {
            register.takeCurrency(tooMuchToTake);
            fail(); // should not get here
        } catch (IllegalArgumentException e) {
            // intentionally empty
        }

        assertThat(register.getBillCounts(), is(equalTo(variousAmounts)));
        assertThat(register.getTotal(), is(BigInteger.valueOf(77)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void takeDoesNotAllowNegativeValues() {
        int[] variousAmounts = {1, 3, 5, 0, 2};
        int[] negativeTake = {1, 3, -2, 0, 1};

        register.putCurrency(variousAmounts);
        register.takeCurrency(negativeTake);
    }

    @Test
    public void negativeTakeAmountDoesNotChangeBillCounts() {
        int[] variousAmounts = {1, 3, 5, 0, 2};
        int[] negativeTake = {1, 3, -2, 0, 1};

        register.putCurrency(variousAmounts);
        try {
            register.takeCurrency(negativeTake);
            fail(); // should not get here
        } catch (IllegalArgumentException e) {
            // intentionally empty
        }

        assertThat(register.getBillCounts(), is(equalTo(variousAmounts)));
        assertThat(register.getTotal(), is(BigInteger.valueOf(77)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void makeChangeDoesNotAllowNegativeValues() {
        int[] variousAmounts = {1, 3, 5, 0, 2};

        register.putCurrency(variousAmounts);
        register.makeChange(BigInteger.valueOf(-10));
    }

    @Test
    public void makeChangeShouldRemoveFromBillCounts() {
        int[] variousAmounts = {1, 3, 5, 1, 2};
        int[] expectedChange = {1, 1, 0, 1, 0};

        register.putCurrency(variousAmounts);
        int[] change = register.makeChange(BigInteger.valueOf(32));
        SimpleGreedyArrayRegister changeRegister = new SimpleGreedyArrayRegister();
        changeRegister.putCurrency(change);

        assertThat(register.getTotal(), is(BigInteger.valueOf(47)));
        assertThat(changeRegister.getTotal(), is(BigInteger.valueOf(32)));
        assertThat(change, is(equalTo(expectedChange)));
    }

    @Test
    public void makeChangeCanEmptyRegister() {
        int[] variousAmounts = {1, 3, 5, 1, 2};

        register.putCurrency(variousAmounts);
        int[] change = register.makeChange(register.getTotal());

        assertThat(change, is(equalTo(variousAmounts)));
        assertThat(register.getTotal(), is(BigInteger.ZERO));
    }

    @Test
    public void makeChangeWillUseVariousBills() {
        int[] variousAmounts = {1, 3, 5, 1, 2};

        register.putCurrency(variousAmounts);
        int[] change = register.makeChange(BigInteger.valueOf(33));

        SimpleGreedyArrayRegister changeRegister = new SimpleGreedyArrayRegister();
        changeRegister.putCurrency(change);
        assertThat(changeRegister.getTotal(), is(equalTo(BigInteger.valueOf(33))));
    }

    @Test(expected = IllegalStateException.class)
    public void makeChangeWillFailWithInsufficientBillsCounts() {
        int[] variousAmounts = {1, 3, 5, 1, 0};

        register.putCurrency(variousAmounts);
        register.makeChange(BigInteger.valueOf(33));
    }

    @Test(expected = IllegalArgumentException.class)
    public void makeChangeWillNotAllowChangeGreaterThanTotal() {
        int[] variousAmounts = {1, 3, 5, 1, 0};

        register.putCurrency(variousAmounts);
        register.makeChange(BigInteger.valueOf(100));
    }

    @Test
    public void makeChangeWithInsufficientBillsLeavesBillCountsUnchanged() {
        int[] variousAmounts = {1, 3, 5, 1, 0};

        register.putCurrency(variousAmounts);
        try {
            register.makeChange(BigInteger.valueOf(33));
            fail(); // should not get here
        } catch (IllegalStateException e) {
            // intentionally empty
        }

        assertThat(register.getBillCounts(), is(equalTo(variousAmounts)));
    }

    @Test
    public void putOrTakeWithMaxIntegerValueShouldBeAllowed() {
        int[] variousAmounts = {Integer.MAX_VALUE, 3, 5, 1, 0};

        register.putCurrency(variousAmounts);
        assertThat(register.getTotal(), is(new BigInteger("42949672997")));

        register.takeCurrency(variousAmounts);
        assertThat(register.getTotal(), is(BigInteger.ZERO));
    }

    @Test
    public void makeChangeWhereGreedyDoesNotWork() {
        int[] variousAmounts = {0, 0, 4, 3, 0};

        register.putCurrency(variousAmounts);
        int[] change = register.makeChange(BigInteger.valueOf(11));
        SimpleGreedyArrayRegister changeRegister = new SimpleGreedyArrayRegister();
        changeRegister.putCurrency(change);

        assertThat(changeRegister.getTotal(), is(equalTo(BigInteger.valueOf(11))));
    }

    @Test
    public void makeChangeWhereGreedyDoesWork() {
        int[] variousAmounts = {0, 0, 1, 0, 3};

        register.putCurrency(variousAmounts);
        int[] change = register.makeChange(BigInteger.valueOf(5));
        SimpleGreedyArrayRegister changeRegister = new SimpleGreedyArrayRegister();
        changeRegister.putCurrency(change);

        assertThat(changeRegister.getTotal(), is(equalTo(BigInteger.valueOf(5))));
    }

    @Test
    public void makeChangeShouldNotUseFirstAvailableBill() {
        int[] variousAmounts = { 0, 0, 1, 4, 0 };

        register.putCurrency(variousAmounts);
        int[] change = register.makeChange(BigInteger.valueOf(8));
        SimpleGreedyArrayRegister changeRegister = new SimpleGreedyArrayRegister();
        changeRegister.putCurrency(change);

        assertThat(changeRegister.getTotal(), is(equalTo(BigInteger.valueOf(8))));
    }

}