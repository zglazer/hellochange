package com.zachglazer.change;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigInteger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class ChangeMachineRunnerTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Mock
    private SimpleGreedyArrayRegister mockRegister;

    private ChangeMachineRunner runner;
    private String expectedState;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        System.setOut(new PrintStream(outContent));

        runner = spy(new ChangeMachineRunner(mockRegister));

        int[] billCounts = { 1, 2, 0, 5, 1};
        expectedState = "$55 1 2 0 5 1";
        when(mockRegister.getBillCounts()).thenReturn(billCounts);
        when(mockRegister.getTotal()).thenReturn(BigInteger.valueOf(55));
    }

    @Test
    public void invalidCommandWillPrintInstructions() {
        mockCommandAndQuit("invalid");

        runner.run();

        assertThatRunnerOutput("unrecognized command\npossible commands: show, put, take, change, help, quit");
    }

    @Test
    public void printRegisterStateShouldBeCorrectInformationInCorrectFormat() {
        mockCommandAndQuit("show");

        runner.run();

        assertThatRunnerOutput(expectedState);
    }

    @Test
    public void putCommandShouldParseCurrencyAndPrintState() {
        mockCommandAndQuit("put 1 1 1 1 1");

        runner.run();

        assertThatRunnerOutput(expectedState);
    }

    @Test
    public void putCommandWillPrintErrorMessageForNonNumericInput() {
        String expectedError = "invalid number provided. only integers are accepted";
        String expectedMessage = expectedError + "\n" + expectedState;
        mockCommandAndQuit("put a b c d e");

        runner.run();

        assertThatRunnerOutput(expectedMessage);
    }

    @Test
    public void putOnlyAcceptsFiveBills() {
        String expectedError = "error occurred during operation: invalid input";
        String expectedMessage = expectedError + "\n" + expectedState;
        mockCommandAndQuit("put 1 2");

        runner.run();

        assertThatRunnerOutput(expectedMessage);
    }

    @Test
    public void takeCommandShouldParseCurrencyAndPrintState() {
        mockCommandAndQuit("take 1 1 1 1 1");

        runner.run();

        assertThatRunnerOutput(expectedState);
    }

    @Test
    public void takeCommandWillPrintErrorMessageForNonNumericInput() {
        String expectedError = "invalid number provided. only integers are accepted";
        String expectedMessage = expectedError + "\n" + expectedState;
        mockCommandAndQuit("take a b c d e");

        runner.run();

        assertThatRunnerOutput(expectedMessage);
    }

    @Test
    public void takeOnlyAcceptsFiveBills() {
        String expectedError = "error occurred during operation: invalid input";
        String expectedMessage = expectedError + "\n" + expectedState;
        mockCommandAndQuit("take 1 2");

        runner.run();

        assertThatRunnerOutput(expectedMessage);
    }

    @Test
    public void changeOnlyAcceptsOneValue() {
        mockCommandAndQuit("change 1 2");

        runner.run();

        assertThatRunnerOutput("invalid number provided. only one integer is accepted");
    }

    @Test
    public void changeWillPrintChangeInBills() {
        mockCommandAndQuit("change 1");
        when(mockRegister.makeChange(any(BigInteger.class))).thenReturn(new int[]{1, 2, 3, 0, 5});

        runner.run();

        assertThatRunnerOutput("1 2 3 0 5");
    }

    @Test
    public void testHelp() {
        mockCommandAndQuit("help");
        when(mockRegister.makeChange(any(BigInteger.class))).thenReturn(new int[]{1, 2, 3, 0, 5});

        runner.run();

        assertThatRunnerOutput("possible commands: show, put, take, change, help, quit");
    }

    private void mockCommandAndQuit(String command) {
        doReturn(command, "quit").when(runner).getInput();
    }

    private void assertThatRunnerOutput(String expected) {
        assertThat(outContent.toString(), is("ready\n" + expected + "\nbye\n"));
    }

}