# Hello Change #

This is a simple implementation of a cash register. It only accepts 20s, 10s, 5s, 2s, and 1 dollar bills.

### Build ###
Build requires JDK 8 to compile. To build project from source type ```./gradlew clean build```. The result artifact will be stored in ```build/lib/simple-1.0-SNAPSHOT.jar```.

### Usage ###
To run the program from the command line type ```java -jar build/lib/simple-1.0-SNAPSHOT.jar```.

Program has the allowing commands:

* `show` -- shows the current bills in the cash register in the following format: `$<total dollar value> <number 20s> <number 10s> <number 5s> <number 2s> <number 1s>`
* `put <number 20s> <number 10s> <number 5s> <number 2s> <number 1s>` - puts the bills in the register
* `take <number 20s> <number 10s> <number 5s> <number 2s> <number 1s>` - takes bills from the register
* `change <dollar amount>` - dollar amount to take from the register. Return the following format: `<number 20s> <number 10s> <number 5s> <number 2s> <number 1s>`
* `help` - prints possible commands
* `quit` - quits the program

### Notes ###
The program only supports up to Integer.MAX_VALUE number of bills for each denomination. Only positive numbers are accepted.